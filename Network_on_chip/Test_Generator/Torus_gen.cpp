/*                                            In The Name Of GOD                               */
// Name : Seyed Sobhan Miryoosfei
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <deque>
#include <set>
#include <vector>
#include <map>
#include <string>
#include <cstring>
#include <iomanip>
#include <cstdio>
#include <fstream>
#include <sstream>

# define For(i, a, b) for(int i=a; i<b; ++i)
# define n(x) (int)x.size()
# define all(x) x.begin(),x.end()
# define pb push_back


# define FF first
# define SS second
# define PB push_back

using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<int,int> PII;
typedef long long LL;

ofstream traffic_out("traffic.txt");
ofstream config_out("config.txt");
int port[]={0,3,4,1,2,6,5};
int dx[]={0,0 ,-1,0 ,+1,0 ,0 };
int dy[]={0,+1,0 ,-1,0 ,0 ,0 };
int dz[]={0,0 ,0 ,0 ,0 ,+1,-1};
int n,cr_delay,num_packets;
int main()
{
	//ios::sync_with_stdio(false);
	cin >> n >> cr_delay >> num_packets;
	srand(time(0));
	config_out<<"num_credit_delay_cycles="<<cr_delay<<endl;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			for(int k=0;k<n;k++){
				int id=i*n*n+j*n+k;
				for(int dir=1;dir<=6;dir++){
					int ni=(i+dx[dir]+n)%n;
					int nj=(j+dy[dir]+n)%n;
					int nk=(k+dz[dir]+n)%n;
					int nid=ni*n*n+nj*n+nk;
					config_out<<id<<":"<<dir<<"-"<<nid<<":"<<port[dir]<<endl;
				}
			}
	for(int id1=0;id1<n*n*n;id1++){
		for(int id2=0;id2<n*n*n;id2++){
			int x1=id1/(n*n);
			int y1=(id1%(n*n))/n;
			int z1=(id1%n);

			int x2=id2/(n*n);
			int y2=(id2%(n*n))/n;
			int z2=(id2%n);

			if(x1!=x2){
				traffic_out<<"route:"<<id1<<"->"<<id2<<":"<<4<<endl;
			}else if(y1!=y2){
				traffic_out<<"route:"<<id1<<"->"<<id2<<":"<<1<<endl;
			}else if(z1!=z2){
				traffic_out<<"route:"<<id1<<"->"<<id2<<":"<<5<<endl;
			}else{
				traffic_out<<"route:"<<id1<<"->"<<id2<<":"<<0<<endl;
			}
		}
		traffic_out<<endl;
	}

	for(int i=0;i<n*n*n;i++){
		int npack=rand()%num_packets;
		if(npack==0)
			continue;
		traffic_out<<"node "<<i<<":"<<npack<<endl;
		for(int j=0;j<npack;j++){
			int next=i;
			while(next==i)
				next=rand()%(n*n*n);
			traffic_out<<i<<":"<<next<<":"<<rand()%4<<":"<<rand()%4+1<<endl;
		}
	}
	return 0;
}
