`include "libraries/MUX.v"
`include "libraries/BUFFER.v"
`include "libraries/TYPE.v"

module router(input[`FLIT_SIZE*`MAX_NUM_IN_PORTS-1:0]  packed_in,
			  output[`FLIT_SIZE*`MAX_NUM_OUT_PORTS-1:0] packed_out,
			  output reg[`MAX_NUM_OUT_PORTS-1:0]            accept_out,
			  input [`MAX_NUM_IN_PORTS-1:0]                 accept_in,
			  //TODO: Repalce const number with define clauses
			  input[(`LOG_NUM_OUT_PORTS*`MAX_NUM_ROUTERS)-1:0] packed_route_table,
			  input[(`MAX_NUM_OUT_PORTS*`MAX_NUM_VCS)-1:0]   packed_credit_in,
			  input[4:0]                               credit_delay,
			  input[1:0] phase,
			  input done_traffic,
			  input clk,
			  input rst,
			  output reg next_flit,
			  output [`MAX_NUM_IN_PORTS*`MAX_NUM_VCS-1:0]   packed_credit_out,
			  output reg done);

	parameter IN_PORTS = 1;
	parameter OUT_PORTS = 1;

	wire[`FLIT_SIZE-1:0] in [`MAX_NUM_IN_PORTS-1 :0];
	reg[`FLIT_SIZE-1:0] out[`MAX_NUM_OUT_PORTS-1:0];

	reg[`FLIT_SIZE-1:0] staging		   [`MAX_NUM_IN_PORTS-1:0];
	reg                 is_full_staging[`MAX_NUM_IN_PORTS-1:0];

	reg[`FLIT_SIZE-1:0] buff           [`MAX_NUM_IN_PORTS-1:0][`MAX_NUM_VCS-1:0];
	reg                 is_full_buffer [`MAX_NUM_IN_PORTS-1:0][`MAX_NUM_VCS-1:0];

	reg mark_in [`MAX_NUM_IN_PORTS-1 :0];
	reg mark_out[`MAX_NUM_OUT_PORTS-1:0];

	reg [`LOG_NUM_IN_PORTS-1:0] cur_inport  [`MAX_NUM_OUT_PORTS-1:0][`MAX_NUM_VCS-1:0];
	reg                        not_reserved[`MAX_NUM_OUT_PORTS-1:0][`MAX_NUM_VCS-1:0];

	reg[`LOG_MAX_CREDIT_DELAY+1:0]            credits      [`MAX_NUM_IN_PORTS-1:0][`MAX_NUM_VCS-1:0];

	wire[`LOG_NUM_OUT_PORTS-1:0]            route_table    [`MAX_NUM_ROUTERS-1:0];

	generate
		genvar g_i,g_j;
		for(g_i=0;g_i<`MAX_NUM_IN_PORTS;g_i=g_i+1)begin
			assign in [g_i]=packed_in[((g_i+1)*`FLIT_SIZE)-1:(g_i*`FLIT_SIZE)];
			assign packed_out[((g_i+1)*`FLIT_SIZE)-1:(g_i*`FLIT_SIZE)]=out[g_i];
			for(g_j=0;g_j<`MAX_NUM_VCS;g_j=g_j+1)
				assign packed_credit_out[g_i*`MAX_NUM_VCS+g_j]=(credits[g_i][g_j]==0 && (!is_full_buffer[g_i][g_j]));
		end
		for(g_i=0;g_i<`MAX_NUM_ROUTERS;g_i=g_i+1)begin
			//TODO: Done replaced by LOG_NUM_IN_PORTS
			assign route_table[g_i]=packed_route_table[(g_i+1)*`LOG_NUM_IN_PORTS-1:g_i*`LOG_NUM_IN_PORTS];
		end
	endgenerate

	integer i,j;
	always @( posedge clk or posedge rst) begin
		if(rst==1)begin
			//TODO: Done / phase is input, it can not changed manualy
			for(i=0;i<`MAX_NUM_IN_PORTS;i=i+1)begin
				//TODO: what is staging!?!? // Buffer in which we temporary save the incoming flit
				staging[i]<=0;
				//TODO: something wrong is here... // a flag that shows if buffer is full or not
				is_full_staging[i]<=0;
				done<=0;
				accept_out[i]<=0;
				next_flit<=0;
				for(j=0;j<`MAX_NUM_VCS;j=j+1)begin
					is_full_buffer[i][j]<=0;
					buff[i][j]<=0;
					credits[i][j]<=0;
					not_reserved[i][j]=1;
					cur_inport[i][j]=0;
				end
			end
		end
		else begin
			begin
				done<=1;
				for(i=0;i<`MAX_NUM_IN_PORTS;i=i+1)begin
					if(is_full_staging[i])begin
						done<=0;
						$display("is_full_staging %d is true-%b",i,is_full_staging[i]);
					end
					if(accept_out[i] || accept_in[i])
						done<=0;
					for(j=0;j<`MAX_NUM_VCS;j=j+1)begin
						if(is_full_buffer[i][j] || credits[i][j]!=0)begin
							done<=0;
						/*
						if(is_full_buffer[i][j])
							$display("is_full_buffer[%d][%d] is true-%b",i,j,is_full_buffer[i][j]);
						if(credits[i][j]!=0)
							$display("credits[%d][%d] is not zero-%d",i,j,credits[i][j]);
						*/
						end
					end
				end
			end
			if(phase==0)begin
				if(next_flit==1)begin
					next_flit<=0;
				end
			end
			if(phase==1)begin
				for(i=0;i<`MAX_NUM_IN_PORTS;i=i+1)begin
					if(i!=0 && is_full_staging[i])begin
						buff[i][staging[i]`VC]<=staging[i];
						is_full_buffer[i][staging[i]`VC]<=1;
						is_full_staging[i]<=0;
						staging[i]<=0;
						$display("flit transfered from staging into the buffer of inport %d and vc %d in router %m",i,staging[i]`VC);
					end
					if(i!=0 && accept_out[i])
						accept_out[i]<=0;
					for(j=0;j<`MAX_NUM_VCS;j=j+1)begin
						if(i!=0 && credits[i][j]!=0)
							credits[i][j]<=credits[i][j]-1;
					end
				end
				if(!done_traffic && !is_full_buffer[0][in[0]`VC])begin
					buff[0][in[0]`VC]<=in[0];
					is_full_buffer[0][in[0]`VC]<=1;
					next_flit<=1;
					$display("flit injected in Router (%m)  is full_buffer %d and VC in_port is %d", is_full_buffer[0][in[0]`VC], in[0]`VC);
				end
			end
			else if(phase==2) begin
				for(i=0;i<`MAX_NUM_IN_PORTS;i=i+1)
					mark_in[i]=0;
				for(i=0;i<`MAX_NUM_OUT_PORTS;i=i+1)
					mark_out[i]=0;
				for(i=0;i<`MAX_NUM_VCS;i=i+1)begin
					for(j=0;j<`MAX_NUM_IN_PORTS;j=j+1)begin
						if(!mark_in[j])begin
							`define fb buff[j][i]
							`define out_port route_table[`fb`dest]
							if(is_full_buffer[j][i])begin
								$display("we are here if 1 with inport %d and vc %d",j,i);
								$display("not reserved is %d and cur inport is %d and mark_out is %d",not_reserved[`out_port][i], cur_inport[`out_port][i], mark_out[`out_port]);
								if(!mark_out[`out_port] && (not_reserved[`out_port][i]||cur_inport[`out_port][i]==j))begin
									if(`out_port==0)begin
										$display("we are here if 2 with inport %d and vc %d",j,i);
										if(`fb`tail_p)begin
											not_reserved[`out_port][i]=1;
										end
										else begin
											not_reserved[`out_port][i]=0;
											cur_inport[`out_port][i]=j;
										end
										is_full_buffer[j][i]<=0;
										mark_in[j]=1;
										mark_out[`out_port]=1;
										if(j!=0)
											credits[j][i]<=credits[j][i]+credit_delay;
										$display("flit extracted in router (%m) from inport %d and vc %d",j,i);
									end else begin
										$display("we are here if 3 with inport %d and vc %d",j,i);
										$display("######## %d",packed_credit_in[`out_port*`MAX_NUM_VCS+i]);
										if(packed_credit_in[`out_port*`MAX_NUM_VCS+i])begin
											if(`fb`tail_p)begin
												not_reserved[`out_port][i]=1;
											end
											else begin
												not_reserved[`out_port][i]=0;
												cur_inport[`out_port][i]=j;
											end
											if(j!=0)
												credits[j][i]<=credits[j][i]+credit_delay;
											mark_in[j]=1;
											mark_out[`out_port]=1;
											out[`out_port]<=`fb;
											is_full_buffer[j][i]<=0;
											accept_out[`out_port]<=1;
											$display("flit has been put on out_port=%d in router (%m)",`out_port);
										end
									end
								end
							end
						end
					end
				end
			end else if(phase==3) begin
				for(i=1;i<`MAX_NUM_IN_PORTS;i=i+1)begin
				//TODO: Done // something wrong is here...
					if(accept_in[i] && !is_full_staging[i])begin
						staging[i]<=in[i];
						is_full_staging[i]<=1;
						$display("flit has saved in staging of inport number %d in router (%m)",i);
					end
				end
			end
		end
		//TODO: Done //phase is input, it can not changed manualy

	end
endmodule
