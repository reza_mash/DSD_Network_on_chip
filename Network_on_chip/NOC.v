
module NoC();
    parameter MAX_NUM_ROUTERS 256
    parameter MAX_NUM_VCS 8
    parameter MAX_NUM_IN_PORTS  16      // including ingress at src
    parameter MAX_NUM_OUT_PORTS  16      // including egress at src
    parameter MAX_NUM_OF_PACKETS_IN_TRAFFIC_BUFFER 1024

    parameter MAX_CYCLE = 1000;
    parameter CREDIT_DELAY_CYCLE = 1;




endmodule
