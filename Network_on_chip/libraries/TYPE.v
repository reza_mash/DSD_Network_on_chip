`define FLIT_T reg [15:0]
`define head_p [0]
`define tail_p [1]
`define VC [4:2]
`define dest [12:5]
//alaki for fun... so filt size became a power of two
`define special [15:13]

`define FLIT_SIZE 16
`define ROUTER_MESH_SIZE 5
/*Temprory changed the values*/ //begin
//256
`define MAX_NUM_ROUTERS 256
//8
`define MAX_NUM_VCS 8
//16
`define MAX_NUM_IN_PORTS 16
//16
`define MAX_NUM_OUT_PORTS 16
//16
`define MAX_CREDIT_DELAY 16
//max number of port * virtual channel
//1024
`define MAX_NUM_OF_PACKETS_IN_TRAFFIC_BUFFER 1024



`define LOG_MAX_CREDIT_DELAY 5
//
`define LOG_NUM_ROUTERS 8
`define LOG_NUM_VCS 3
//4
`define LOG_NUM_IN_PORTS 4
//4
`define LOG_NUM_OUT_PORTS 4
`define LOG_NUM_OF_PACKETS_IN_TRAFFIC_BUFFER 10
//end


`define MAX_CYCLE 1000000
`define CREDIT_DELAY_CYCLE 1

`define PORT_NUM 7
