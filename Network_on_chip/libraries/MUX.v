module MUX2(out, select, a, b);
	parameter INPUT_SIZE = 1;
	output wire	[INPUT_SIZE-1:0] out;
	input wire	[INPUT_SIZE-1:0] a, b;
	input wire	select;
	
	assign out = 	select==1'b0 ? a : b;
endmodule

module MUX4(out, select, a, b, c, d);
	parameter INPUT_SIZE = 1;	
	output wire	[INPUT_SIZE-1:0] out;
	input wire	[INPUT_SIZE-1:0] a, b, c, d;
	input wire	[1:0] select;
	
	assign out =	select==2'b00 ? a : 
					select==2'b01 ? b :
					select==2'b10 ? c : d;
endmodule

module MUX8(out, select, in1, in2, in3, in4, in5, in6, in7, in8);
	parameter INPUT_SIZE = 1;	
	output wire	[INPUT_SIZE-1:0] out;
	input wire	[INPUT_SIZE-1:0] in1, in2, in3, in4, in5, in6, in7, in8;
	input wire	[2:0] select;
	
	assign out =	select==3'b000 ? in1 : 
					select==3'b001 ? in2 :
					select==3'b010 ? in3 :
					select==3'b011 ? in4 :
					select==3'b100 ? in5 :
					select==3'b101 ? in6 :
					select==3'b110 ? in7 : in8;
endmodule

module MUX16(out, select, in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16);
	parameter INPUT_SIZE = 1;	
	output wire	[INPUT_SIZE-1:0] out;
	input wire	[INPUT_SIZE-1:0] in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16;
	input wire	[3:0] select;
	
	assign out =	select==4'b0000 ? in1 : 
					select==4'b0001 ? in2 :
					select==4'b0010 ? in3 :
					select==4'b0011 ? in4 :
					select==4'b0100 ? in5 :
					select==4'b0101 ? in6 :
					select==4'b0110 ? in7 :
					select==4'b0111 ? in8 :
					select==4'b1000 ? in9 :
					select==4'b1001 ? in10:
					select==4'b1010 ? in11:
					select==4'b1011 ? in12:
					select==4'b1100 ? in13:
					select==4'b1101 ? in14:
					select==4'b1110 ? in15: in16;
endmodule
