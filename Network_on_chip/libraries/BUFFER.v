/*WARNING: NOT COMPLETE, DO NOT USE THIS MODULE*/

/*
	Parameterized Buffer
	in every positive edge of each clock, read the buffer
	///////////////////////////////////////////////////////////////////////////////
											*/


/*WARNING: NOT COMPLETE, DO NOT USE THIS MODULE*/											
module BUFFER(out, write_sig, read_sig, inp, clock, reset);
	parameter MEM_LENGTH=1;			//length of each buffer cell
	parameter LOG_SIZE = 8;			//actual size of buffer is 2 power this number (Minimum 1)
	output wire [MEM_LENGTH-1:0] out;
	input wire write_sig, read_sig, clock, reset;
	input wire [MEM_LENGTH-1:0] inp;

	integer i;
	reg [MEM_LENGTH-1:0] buff_cells [(1<<LOG_SIZE)-1:0];
	reg [LOG_SIZE-1:0] read_point, write_point;
	
	always @(posedge clock)
		if(reset == 1'b1) begin
			for(i=0; i<(1<<LOG_SIZE); i=i+1)
				buff_cells[i] = 0;
			read_point = 0;
			write_point = 0;
		end else begin
			//////////////////////////////////////////////////////////////////////////
		end
			
	
	
endmodule