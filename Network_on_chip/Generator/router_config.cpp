/*                                            In The Name Of GOD                               */
// Name : Seyed Sobhan Miryoosfei
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <deque>
#include <set>
#include <vector>
#include <map>
#include <string>
#include <cstring>
#include <iomanip>
#include <cstdio>
#include <fstream>
#include <sstream>

# define For(i, a, b) for(int i=a; i<b; ++i)
# define n(x) (int)x.size()
# define all(x) x.begin(),x.end()
# define pb push_back


# define FF first
# define SS second
# define PB push_back

using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<int,int> PII;
typedef long long LL;

const int MAX_NUM_ROUTERS=1000+10;
int num_of_routers=0;

void read_and_init_routers(char* conf_file){
    FILE *router_configuration_file = fopen(conf_file, "r");
    int ret;
    char str[100];
    int src_router, src_out_port, dest_router, dest_in_port;
    int num_credit_delay_cycles;
    while (fgets(str, 100, router_configuration_file) != NULL) {
      if ((ret = sscanf(str, "num_credit_delay_cycles=%d", &num_credit_delay_cycles)) == 1) {
        printf("assign CR_DELAY=%d;\n", num_credit_delay_cycles);

      } else if ((ret = sscanf(str, "%d:%d-%d:%d", &src_router, &src_out_port, &dest_router, &dest_in_port)) == 4) {
        printf("assign in_ports[%d][(`FLIT_SIZE*%d)-1:(`FLIT_SIZE*%d)]=out_ports[%d][(`FLIT_SIZE*%d)-1:(`FLIT_SIZE*%d)];\n",dest_router,dest_in_port+1,dest_in_port,src_router,src_out_port+1,src_out_port);
        printf("assign credit_in[%d][(`MAX_NUM_VCS*%d)-1:(`MAX_NUM_VCS*%d)]=credit_out[%d][(`MAX_NUM_VCS*%d)-1:(`MAX_NUM_VCS*%d)];\n",dest_router,dest_in_port+1,dest_in_port,src_router,src_out_port+1,src_out_port);
        printf("assign accept_in[%d][%d]=accept_out[%d][%d];\n",dest_router,dest_in_port,src_router,src_out_port);
        num_of_routers=max(num_of_routers,src_router+1);
        num_of_routers=max(num_of_routers,dest_router+1);
      }
    }
    fclose(router_configuration_file);
    return;
}
struct flit{
    int special;
    int dest;
    int vc;
    bool is_tail;
    bool is_head;
    flit(int a,int b,int c,int d,int e){
        special=a;
        dest=b;
        vc=c;
        is_tail=d;
        is_head=e;
    }
    const char* toVerilog(){
        ostringstream buff;
        buff<<"{"<<"3'd"<<special<<",";
        buff<<"8'd"<<dest<<",";
        buff<<"3'd"<<vc<<",";
        buff<<"1'b"<<is_tail<<",";
        buff<<"1'b"<<is_head<<"}";
        string ret=buff.str();
        char *cstr=new char[ret.size()+10];
        memcpy(cstr, ret.c_str(), ret.size());
        return cstr;
    }
};
struct packet{
    int src;
    int dest;
    int vc;
    int num_flits;
    packet(int a,int b,int c,int d){
        src=a;
        dest=b;
        vc=c;
        num_flits=d;
    }
    flit get_flit(int num){
        return flit(0,dest,vc,num==num_flits-1,num==0);
    }
};
vector<packet> packets[MAX_NUM_ROUTERS];
vector<flit> flits[MAX_NUM_ROUTERS];
int num_packets_to_send [MAX_NUM_ROUTERS];
void read_and_init_traffic(char* traff_file){
    FILE *traffic_file = fopen(traff_file, "r");
    char str[100];
    while (fgets(str, 100, traffic_file) != NULL) {
      int src;
      int dest;
      int num_flits;
      int ret;
      int input_num_packets_to_send;
      int verbose;
      int vc;
      int out_port;

      if ((str[0] == '%') || (str[0] == '\n')) {

      } else if ((ret = sscanf(str, "route:%d->%d:%d", &src, &dest, &out_port)) == 3) {
        printf("assign router_table[%d][%d:%d]=4'd%d;\n",src,(dest+1)*4-1,dest*4,out_port);
        num_of_routers=max(num_of_routers,src+1);
        num_of_routers=max(num_of_routers,dest+1);
        } else if ((ret = sscanf(str, "node %d:%d\n", &src, &input_num_packets_to_send)) == 2) {
        num_packets_to_send[src]=input_num_packets_to_send;
        num_of_routers=max(num_of_routers,src+1);
      } else if ((ret = sscanf(str, "%d:%d:%d:%d\n", &src, &dest, &vc, &num_flits)) == 4) {
        packets[src].PB(packet(src,dest,vc,num_flits));
        num_of_routers=max(num_of_routers,src+1);
        num_of_routers=max(num_of_routers,dest+1);
      } else if (!strncmp("end", str, 3)) {
        // provides an optional way to terminate reading the traffic file
        break;
      }
    }
    fclose(traffic_file);
    printf("\n");
    for(int i=0;i<num_of_routers;i++){
        int pos=0;
        for(int j=0;j<num_packets_to_send[i];j++){
            packet now=packets[i][j%packets[i].size()];
            int num_flits=now.num_flits;
            for(int k=0;k<num_flits;k++){
                printf("assign traffic[%d][%d]=%s;\n",i,pos,now.get_flit(k).toVerilog());
                pos++;
            }
        }
        printf("assign traffic[%d][%d]=%s;\n",i,pos,"{`FLIT_SIZE{1'b1}}");
        printf("\n");
    }
    return;
}
int main(int argc, char *argv[])
{
	ios::sync_with_stdio(false);
    if (argc != 3) {
      printf("usage: router_configuration_file traffic_file\n");
      exit(1);
    }
    read_and_init_routers(argv[1]);
    printf("\n");
    printf("parameter num_routers=%d;\n",num_of_routers);
    printf("\n");
    read_and_init_traffic(argv[2]);

	return 0;
}
