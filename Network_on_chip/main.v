//`include "libraries/MUX.v"
//`include "libraries/BUFFER.v"
`include "libraries/TYPE.v"
//TODO: 4 stage -> 3 stage
module Main(output reg [10:0] ans, output wire temp,
	input wire clock, reset);
	
	
	parameter VC = 10;
	wire [4:0] CR_DELAY;
	reg [1:0] stage;
	reg done [`MAX_NUM_ROUTERS-1:0];
	reg [15:0]	cycle;
	reg all_done 	;
	wire done_router [`MAX_NUM_ROUTERS-1:0];
	wire [`FLIT_SIZE*`MAX_NUM_IN_PORTS-1:`FLIT_SIZE] in_ports			[`MAX_NUM_ROUTERS-1:0];
	wire  [`FLIT_SIZE-1:0]	cpu_in_port									[`MAX_NUM_ROUTERS-1:0];
	
	wire [`FLIT_SIZE*`MAX_NUM_OUT_PORTS-1:0] out_ports 		[`MAX_NUM_ROUTERS-1:0];
	
	
	wire [`MAX_NUM_IN_PORTS-1:0] accept_in  							[`MAX_NUM_ROUTERS-1:0];
	wire [`MAX_NUM_OUT_PORTS-1:0] accept_out							[`MAX_NUM_ROUTERS-1:0];
	
	wire [`MAX_NUM_OUT_PORTS*`MAX_NUM_VCS-1:0] credit_in					[`MAX_NUM_ROUTERS-1:0];
	wire [`MAX_NUM_OUT_PORTS*`MAX_NUM_VCS-1:0] credit_out					[`MAX_NUM_ROUTERS-1:0];
	
	wire [(`LOG_NUM_OUT_PORTS*`MAX_NUM_ROUTERS)-1:0] router_table 			[`MAX_NUM_ROUTERS-1:0];
	wire  [(`LOG_NUM_ROUTERS+`LOG_NUM_OUT_PORTS)-1:0] configs [`MAX_NUM_ROUTERS-1:0][`MAX_NUM_IN_PORTS-1:0];
	wire  [`FLIT_SIZE-1:0] traffic [`MAX_NUM_ROUTERS-1:0][`MAX_NUM_OF_PACKETS_IN_TRAFFIC_BUFFER-1:0];
	reg	 [`LOG_NUM_OF_PACKETS_IN_TRAFFIC_BUFFER-1:0] traffic_pointer [`MAX_NUM_ROUTERS-1:0];
	/*Credit For Cpu*/
	wire send_cpu_flit 									[`MAX_NUM_ROUTERS-1:0];
	
	assign temp = send_cpu_flit[0];
	`include "Generator/generated_code.txt"
	//Read Traffic configuration and data
	initial begin
			//$readmemh("config.txt" , configs);
		//$readmemb("traffic.txt", traffic);
		//This file is filled with zeroes, so the pointers be zero
	//	$readmemh("traffic_pointer.txt", traffic_pointer);	
	//	$readmemh("router_table.txt", router_table);
		//$readmemh("credit_delay.txt", CR_DELAY);
	end
	
	genvar r, p; // r stands for router, p stands for port
	
	generate
		for(r=0; r<`MAX_NUM_ROUTERS; r=r+1)begin
			router myrouter(
						.packed_in({in_ports[r], cpu_in_port[r]}),
						.packed_out(out_ports[r]),
						.done_traffic(done[r]),
						.clk(clock),
						.credit_delay(CR_DELAY),
						.phase(stage),
						.next_flit(send_cpu_flit[r]),
						.packed_credit_in(credit_in[r]),
						.packed_credit_out(credit_out[r]),
						.rst(reset),
						//TODO:
						.accept_in(accept_in[r]),
						.accept_out(accept_out[r]),
						.packed_route_table(router_table[r]),
						.done(done_router[r])
						);	
	end endgenerate
	
	generate for(r=0; r<`MAX_NUM_ROUTERS; r=r+1)
			 assign cpu_in_port[r]=traffic[r][traffic_pointer[r] ];
	endgenerate		
	
	integer i;
	always @(posedge clock)begin
		if(reset==1)begin
			stage <= 2'b00;
			//TODO 0 --> n'b0
			cycle <= 16'b0;
			all_done = 1'b0; 
			ans <= 0;
			for(i=0; i< `MAX_NUM_ROUTERS; i=i+1)begin
				done[i] = 1'b0;
				//TODO:
				traffic_pointer[i] = 0;
			end
		end
		else begin
			stage <= stage+1;
			//TODO: case instead of if
			//TODO: Parametrized the states of stage
			if(stage == 2'b00) begin
				for(i=0; i< `MAX_NUM_ROUTERS; i=i+1)begin
					//$display("next_flit of %d is %b in cycle %d (done %b)", i, send_cpu_flit[i], cycle, done[i]);
					if(traffic_pointer[i] == `LOG_NUM_OF_PACKETS_IN_TRAFFIC_BUFFER'b11_1111_1111
							|| traffic[i][traffic_pointer[i] ]`special !=0)
							done[i] = 1'b1;
					if(send_cpu_flit[i])begin
						if(done[i]==1'b0) begin
							traffic_pointer[i]=traffic_pointer[i]+1;
							//$display("%b %b", traffic[i][traffic_pointer[i] ]`special, traffic_pointer[i]);
							//$display ("Import flit %b_%b_%b_%b%b to %d in cycle: %d",
							//traffic[i][traffic_pointer[i] ]`special,
							//traffic[i][traffic_pointer[i] ]`dest,
							//traffic[i][traffic_pointer[i] ]`VC,
							//traffic[i][traffic_pointer[i] ]`head_p,
							//traffic[i][traffic_pointer[i] ]`tail_p,
							//i, cycle);
							if(traffic_pointer[i] == `LOG_NUM_OF_PACKETS_IN_TRAFFIC_BUFFER'b11_1111_1111
								|| traffic[i][traffic_pointer[i] ]`special !=0)
								done[i] = 1'b1;							
						end
					end
					
					
				end
			end
			if(stage == 2'b00)begin
				all_done = 1'b1;
				for(i=0; i< num_routers; i=i+1)begin
					//$display("is_done:%b  of router %d", done_router[i], i);
					//TODO: add traffic dones into the if
					if(done_router[i] == 1'b0 || done[i] == 1'b0)
						all_done = 1'b0;
				end
				if(ans==0 && all_done == 1'b1)
					ans <= cycle-1;
				if(ans==0)
					$display("we are in cycle %d", cycle);
			end
			if(stage == 2'b11)begin
				cycle <= cycle +1;
			end
		end
	end
	
endmodule